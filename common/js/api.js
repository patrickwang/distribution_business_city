var app = getApp();
let request = (_url,_type,_data,callback) => {
  // 获取本地保存的信息 
  var userInfo = wx.getStorageSync('userInfo');
  wx.request({
    url: 'https://www.weiyoho.com/api' + _url,
    data:_data,
    method:_type,
    header:{
      appletId: '47cbcc3aef82744ba6ec00737ed1c8a7'
    },
    success:((res) => {
      if (res.statusCode == 200){
        callback(res)
      }
    }),
    fail:((res) => {
      console.log(res)
    })
  })
}
module.exports = {
  request: request
}