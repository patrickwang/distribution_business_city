var sliderWidth = 76; // 需要设置slider的宽度，用于计算中间位置

Page({
  data: {
    tabs: ["热销", "新品"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
    collect:false,
    carefullys: ['http://wx.weiyoho.com/images/product-vg@1.png', 'http://wx.weiyoho.com/images/product-dryfruit@1.png', 'http://wx.weiyoho.com/images/product-rice@1.png', 'http://wx.weiyoho.com/images/product-tea@1.png', 'http://wx.weiyoho.com/images/product-dryfruit@2.png', 'http://wx.weiyoho.com/images/product-cake@2.png', 'http://wx.weiyoho.com/images/product-vg@2.png', 'http://wx.weiyoho.com/images/product-dryfruit@3.png']
  },
  // 跳转详情页面
  to_detail() {
    wx.navigateTo({
      url: '../../detail/index/index'
    })
  },
  // 收藏
  to_collect(){
    var that = this;
    var collect = this.data.collect;
    that.setData({
      collect: !collect
    })
    wx.showToast({
      title: '操作成功',
      icon: 'success',
      duration: 2000
    })
    
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        console.log(res.windowWidth)
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  }
});