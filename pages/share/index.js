// pages/share/index.js
import api from "../../utils/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    qrcode: "",
    imgList: [],
    imgDefault: false,
  },
  getImg(params) {
    let that = this;
    let imgList = [];
    wx.showLoading({
      title: '加载中',
    })
    api.request("/user/qrcode", "POST", params, (res) => {
      console.log(res);
      imgList.push(res.data.qrcode.url);
      console.log(imgList);
      that.setData({
        imgDefault: true,
        qrcode: res.data.qrcode.url,
        imgList: imgList
      });
      wx.hideLoading();
    })
  },
  // 图片预览
  previewImage: function() {
    wx.previewImage({
      current: this.data.qrcode, // 当前显示图片的http链接  
      // urls: this.data.imgalist // 需要预览的图片http链接列表 
      urls: this.data.imgList
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getImg(options)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})