//商品详情

import api from '../../../utils/api'
var app = getApp();
var host = app.globalData.host;
var globalData = app.globalData;
let animationShowHeight = 300;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    host: getApp().globalData.host,
    isloadmain: false,
    num: 1,
    show_top: false,
    show_down: true,
    isSelect: 0, //品类默认选中项
    height: 0,
    curIndex: 0,
    to_detail: true,
    details: {},
    collect_arr: [],
    banners: [],
    animationData: {},
    showModalStatus: false,
    spec_name: [],
    spec_price: [],
    carts: [], //购物车物品
    show_badge: false, //badge的显示
    type: '',
    sel_price: 0,
    more_com: true, //是否有更多评论
    isSignIn: false, //是否登录
    userInfo: {}, //用户信息
    scene: {} //二维码参数
  },
  /**
   * 获取用户数据
   */
  getUserInfo() {
    let that = this;
    let friend_id = this.data.scene.user_id;
    api.request('/user/info', 'GET', {}, (res) => {
      if (res.data.status === 0) {
        wx.setStorageSync('USER_INFO', res.data.user_info);
        that.setData({
          userInfo: res.data.user_info,
        })
        //如果通过二维码进来则建立联系
        if (friend_id) {
          that.buildRelationship(friend_id)
        }
      } else {
        wx.showModal({
          content: res.data.meg,
        })
      }
    })
  },
  //建立联系
  buildRelationship(id) {
    api.request('/user/relation', 'POST', {
      friend_id: id
    }, res => {
      if (res.data.status == 0) {
        // wx.showModal({
        //   title: '提示',
        //   content: '建立关系成功',
        // })
      } else {
        // wx.showModal({
        //   title: '提示',
        //   content: res.data.msg,
        // })
      }
    })
  },
  _login: function() {
    let that = this;
    wx.login({
      success: function(ret) {
        var code = ret.code;
        var data = {
          "code": code
        };
        api.request('/user/login_code', 'POST', data, (res) => {
          wx.setStorageSync('USER_TOKEN', res.data.data.token);
          that.getUserInfo();
        });
      }
    })
  },

  //token校验
  _checkToken() {
    var that = this;
    var userToken = wx.getStorageSync('USER_TOKEN');
    api.request('/user/check', 'POST', {
      token: userToken
    }, (res) => {
      console.log(res.data.check);
      var checkStatus = res.data.check;
      //如果校验失败
      if (!checkStatus) {
        that._login();
        console.log('校验失败');
      } else {
        that.getUserInfo();
      }
    }, (res) => {
      wx.showModal({
        title: '错误',
        content: '服务器检测token错误',
      })
    });
  },
  _autoLogin: function () {
    var userInfo = wx.getStorageSync('USER_INFO');
    var friend_id = this.data.scene.user_id;
    if (!userInfo) {
      this.setData({
        isSignIn: false
      })
      // 如果有二维码参数则提示登录
      if (friend_id) {
        wx.showModal({
          title: '提示',
          content: '请先登录',
          success() {
            //跳转至个人中心
            wx.switchTab({
              url: '/pages/parsonal/index/index',
            })
          }
        })
      }
    } else {
      //token校验
      this._checkToken();
      this.setData({
        isSignIn: true
      })
    }
  },

  /* 减少数量 */
  bindMinus: function() {
    var num = this.data.num;
    // 如果大于1时，才可以减 
    if (num > 1) {
      num--;
    }
    // 只有大于一件的时候，才能normal状态，否则disable状态 
    var minusStatus = num <= 1 ? 'disabled' : 'normal';
    // 将数值与状态写回 
    this.setData({
      num: num,
      minusStatus: minusStatus
    });
  },

  /* 增加数量 */
  bindPlus: function() {
    var num = this.data.num;
    // 不作过多考虑自增1 
    num++;
    // 只有大于一件的时候，才能normal状态，否则disable状态 
    var minusStatus = num < 1 ? 'disabled' : 'normal';
    // 将数值与状态写回 
    this.setData({
      num: num,
      minusStatus: minusStatus
    });
  },

  // 显示规格
  show_spec() {
    this.setData({
      show_top: !this.data.show_top,
      show_down: !this.data.show_down
    })
  },

  //选择品类
  check_spec(e) {
    let spec_price = this.data.spec_price;
    let idx = e.currentTarget.dataset.index;
    let spec_type = e.currentTarget.dataset.type;
    this.setData({
      isSelect: idx,
      sel_price: spec_price[idx],
      type: spec_type
    })
  },

  // 切换选项卡
  bindTab(e) {
    this.setData({
      curIndex: e.currentTarget.dataset.index
    })
  },
  to_shop() {
    wx.switchTab({
      url: '/pages/home/index/index'
    })
  },
  //去登录
  goToLogin() {
    wx.showModal({
      title: '提示',
      content: '请登录之后再进行其他操作',
      showCancel: false,
      success(res) {
        wx.switchTab({
          url: '/pages/parsonal/index/index',
        })
      }
    })
  },

  // 分享
  to_share() {
    let that = this;
    let pro_id = this.data.details.id;
    console.log(pro_id)
    if (this.data.isSignIn === false) {
      that.goToLogin();
      return;
    }
    wx.navigateTo({
      url: `/pages/share/index?page=pages/detail/index/index&product_id=${pro_id}`,

    })
  },


  // 判断购物车是否有商品，控制badge的显示
  get_carts() {
    let val = wx.getStorageSync('carts');
    if (val.length == 0) {
      this.setData({
        show_badge: false
      })
    } else {
      this.setData({
        show_badge: true
      })
    }

  },

  // 加入购物车
  addTo_cart(e) {
    var that = this;
    let detail = e.currentTarget.dataset.detail;
    // 查找重复的
    var repeat = this.data.carts.find((item, index) => {
      return item.id == detail.id && (item.spec_id == that.data.isSelect);
    })

    if (repeat) {
      // 如果有重复的
      let indx = this.data.carts.indexOf(repeat);
      this.data.carts[indx].num += that.data.num;
      wx.setStorageSync('carts', this.data.carts)

    } else {
      // 如果没有重复的
      let cart_item = {
        'id': detail.id,
        'pic': detail.pro_image,
        'name': detail.pro_name,
        'price': this.data.sel_price,
        'selected': false,
        'spec_type': this.data.type,
        'num': this.data.num,
        'spec_id': this.data.isSelect,
        'freight_id': detail.freight_id,
        'freight': detail.freight
      }
      this.data.carts.push(cart_item);
      wx.setStorageSync('carts', this.data.carts)
    }
    this.setData({
      showModalStatus: false
    })
    wx.showToast({
      title: '添加成功',
      icon: 'success',
      duration: 2000
    })
  },

  // 立即购买
  to_buy(e) {
    console.log(e.currentTarget.dataset.item)
    var that = this;
    let pro = [];
    let pro_item = {
      "id": e.currentTarget.dataset.item.id,
      "name": e.currentTarget.dataset.item.pro_name,
      'pic': e.currentTarget.dataset.item.pro_image,
      "spec_id": that.data.isSelect,
      "num": that.data.num,
      'price': that.data.sel_price,
      'spec_type': that.data.type,
      'freight_id': e.currentTarget.dataset.item.freight_id,
      'freight': e.currentTarget.dataset.item.freight
    }
    pro.push(pro_item);
    if (that.data.isSignIn === false) {
      wx.setStorageSync("pro_id", that.data.details.id);
      that.goToLogin();
      return;
    }
    wx.setStorage({
      key: "pro",
      data: pro
    })
    globalData.paymentPage = "detail"
    wx.navigateTo({
      url: '../../payment/index/index'
    })
  },

  // 显示遮罩
  showModal: function(e) {
    let that = this;
    if (this.data.userInfo.level == 0) {
      if (e.currentTarget.dataset.op == 'buy') {
        this.setData({
          to_detail: true
        })
      } else {
        this.setData({
          to_detail: false
        })
      }
      this.setData({
        showModalStatus: true
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '您未登录或没有权限进行该操作',
      })
    }
  },

  // 隐藏遮罩层 
  hideModal: function() {
    this.setData({
      showModalStatus: false
    })
  },

  // 获取商品详情
  _load_details(detail_id) {
    var that = this;
    api.request('/shop/product', 'GET', {
      id: detail_id
    }, (res) => {
      console.log(res.data.data);
      that.setData({
        details: res.data.data,
        banners: res.data.data.pro_slides,
        comment: res.data.comment.data,
        spec_name: res.data.data.spec_name,
        spec_price: res.data.data.spec_price,
        sel_price: res.data.data.spec_price[0],
        type: res.data.data.spec_name[0],
        more_com: res.data.comment.has_more,
        isloadmain: true
      })
    })
  },

  // 图片预览
  previewImage: function(e) {
    var that = this;
    let pro_images = this.data.details.pro_images;
    for (let i = 0; i < pro_images.length; i++) {
      console.log(that.data.host + pro_images[i])
      pro_images[i] = that.data.host + pro_images[i]
    }
    var current = e.target.dataset.src;
    wx.previewImage({
      current: current, // 当前显示图片的http链接  
      // urls: this.data.imgalist // 需要预览的图片http链接列表 
      urls: pro_images
    })
  },
  //字符串转换对象
  parseQueryString(url) {
    let items = url.split("&"); //分割成数组
    let params = {};
    let arr, name, value;
    for (var i = 0; i < items.length; i++) {
      arr = items[i].split("="); //["key0", "0"]
      name = arr[0];
      value = arr[1];
      params[name] = value;
    }
    return params
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let scene = decodeURIComponent(options.scene);
    let scene2 = this.parseQueryString(scene);
    let userInfo = wx.getStorageSync("USER_INFO");
    let carts = wx.getStorageSync('carts');
    let that = this;
    let detail_id = options.id || scene2.product_id; //为二维码参数或页面参数
    wx.setStorageSync("scene", scene);
    this.setData({
      scene: scene2
    })
    this._autoLogin();
    this._load_details(detail_id);

    // if (scene != "undefined") { //如果通过二维码进入
    //   let scene2 = this.parseQueryString(scene); //字符串参数转为对象
    //   if (userInfo) { //已登录
    //     this.setData({
    //       scene: scene2
    //     })
    //     this._load_details(scene2.product_id);
    //   } else { //未登录
    //     this._load_details(scene2.product_id);
    //     wx.setStorageSync("scene", scene);
    //     wx.showModal({
    //       title: '提示',
    //       content: '请登录之后在进行其他操作',
    //       success() {
    //         wx.switchTab({
    //           url: '/pages/parsonal/index/index',
    //         })
    //       }
    //     })
    //   }
    // } else {
    //   this._load_details(detail_id)
    // }
    // //如果用户已登录
    // if (userInfo) {
    //   this._checkToken();
    //   this.setData({
    //     isSignIn: true,
    //     userInfo: userInfo
    //   })
    // }
    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          height: res.windowHeight - 101 * 0.42 + 'px'
        })
      }
    })
    //获取购物车物品
    if (carts) {
      that.setData({
        carts: carts
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let that = this;
    wx.getSystemInfo({
      success: function(res) {
        animationShowHeight = res.windowHeight;
      }
    })
    that.setData({
      showModalStatus: false
    })
    this.get_carts()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})