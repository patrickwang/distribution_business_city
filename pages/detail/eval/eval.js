// 商品详情评价
import api from '../../../utils/api'
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    host: getApp().globalData.host,
    comment:[],
    hasMore: true,
    total:0,
    pageNo: 1,
    pro_id:-1
  },
  // 获取商品评价列表
  _get_commonts(pro_id){
    var that = this;
    api.request('/comment/list', 'GET', {
      pro_id: that.data.pro_id,
      page:that.data.pageNo
    }, (res) => {
      console.log(res.data.total)
      var comment = that.data.comment;
      if (res.data.data.length) {
        for (var i = 0; i < res.data.data.length; i++) {
          comment.push(res.data.data[i]);
        }
      }
      that.setData({
        comment: comment,
        total:res.data.total,
        hasMore: res.data.has_more//判断是否有更多
      })
    })
  },

  //滚动到底部
  onscrolltolower: function () {
    var that = this;
    that.data.pageNo++;
    if (that.data.hasMore) {
      that._get_commonts(this.data.pro_id)
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      pro_id: options.id
    })
    this._get_commonts(this.data.pro_id);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})