// pages/opencard/opencard.js
// 开卡
import api from '../../utils/api'

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  openCard:function(){

    wx.showLoading()
    wx.openCard({
      cardList: [
        {
          cardId: "pWhGnjrHxK6v9CMIrtu2BK_VBkQ0",
          code: "329446681841"
        }
      ],
      success: function (res) {
        console.log(res)
      },
      complete: function () {
        wx.hideLoading()
      }
    })
  },

  //提示
  _showModal(msg) {
    wx.showToast({
      title: msg,
      image: '/images/icon/baseclose.jpg',
      duration: 2000
    })
  },
  
  // 保存
  formSubmit: function (e) {
    var that = this;
    let formData = e.detail.value;
    formData.default = that.data.ischecked === true ? '1' : '0';

    // 表单验证
    if (formData.mobile == '') {
      that._showModal('请输入手机号')
      return;
    }
    if (formData.verify == '') {
      that._showModal('请输入验证码')
      return;
    }
    if (formData.name == '') {
      that._showModal('请输入姓名')
      return;
    }
    console.log('form携带数据为：', formData);
      // 添加地址
    api.request('/address/save?XDEBUG_SESSION_START=PHPSTORM', 'POST', formData, (res) => {
      wx.showToast({
        title: res.data.msg,
        icon: 'success',
        duration: 1000
      })
      setTimeout(function () {
        wx.navigateBack()
      }, 1500)
    });

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ params: options });
    console.log("接收到的参数是str=");
    console.log(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})