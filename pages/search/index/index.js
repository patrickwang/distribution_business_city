Page({

  /**
   * 页面的初始数据
   */
  data: {
    content:'',
    search_arr:[],
    active:''
  },
  // 获取输入框的值
  getInput(e){
    if(e.detail.value){
      this.setData({
        active:'search-active',
        content: e.detail.value
      })
    }else{
      this.setData({
        active: ''
      })
    }
  },
  
  // 搜索
  to_search(){
    var that = this;
    let s_cont = that.data.content;
    if (s_cont != ''){
      let ifarr = that.data.search_arr.indexOf(s_cont) > -1;
      if (!ifarr){
        // 插入数组
        that.data.search_arr.push(s_cont);
        // 本地缓存数组
        wx.setStorageSync('search_arr', that.data.search_arr);
      }
      wx.redirectTo({
        url: '/pages/category/classify/classify?pro_name=' + s_cont
      })
    }else{
      wx.showToast({
        title: '输入搜索内容',
        image: '/images/icon/baseclose.jpg',
        duration: 2000
      })
    }
  },

  // 清除历史搜索记录
  clear_seararr(){
    wx.removeStorageSync('search_arr');
    this.setData({
      search_arr:[]
    })
  },

  // 点击历史记录
  gethistory(e){
    let hisitem = e.currentTarget.dataset.item;
    this.setData({
      content: hisitem,
      active: 'search-active',
      inputTxt: hisitem
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取本地缓存的search_arr
    let arr = wx.getStorageSync('search_arr');
    if (arr.length){
      this.setData({
        search_arr: arr
      })
    }else{
      this.setData({
        search_arr: []
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})