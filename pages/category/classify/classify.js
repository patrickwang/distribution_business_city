//分类商品列表 

import api from '../../../utils/api'
var app = getApp();
var host = app.globalData.host;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    host:host,
    hasMore: true,
    pageNo: 1,
    list: [],
    content: '',//搜索框的值
    p_name:''
  },
  // 跳转详情页面
  to_detail(){
    wx.navigateTo({
      url: '../../detail/index/index'
    })
  },

  //获取搜索列表
  _load_searchlist(name){
    var that = this;
    api.request('/shop/pro_search', 'GET', {
      page: that.data.pageNo,
      row: 10,
      pro_name:name
    }, (res) => {
      console.log(res)
      var list = that.data.list;
      if (res.statusCode == 200) {
        if (res.data.data.length) {
          for (var i = 0; i < res.data.data.length; i++) {
            list.push(res.data.data[i]);
          }
        }
        that.setData({
          list: list,
          hasMore: res.data.has_more//判断是否有更多
        })
      }
    })
  },

  //滚动到底部
  onscrolltolower: function () {
    var that = this;
    that.data.pageNo++;
    if (that.data.hasMore) {
      that._load_searchlist()
    }
  },

  // 获取输入框的值并搜索
  getInput(e) {
    var that = this;
    if (e.detail.value) {
      that.setData({
        content: e.detail.value,
        list:[],
        pageNo:1
      })
      let name = that.data.content;
      that._load_searchlist(name);
    } else {
      that.setData({
        list: [],
        pageNo: 1
      })
      // 搜索值为空时，默认初始值
      that._load_searchlist(that.data.p_name);
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.pro_name);
    let p_name = options.pro_name;
    this.setData({
      p_name: p_name
    })
    this._load_searchlist(p_name)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})