//分类

import api from '../../../utils/api'
var app = getApp();
var host = app.globalData.host;
var globalData = app.globalData;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    host: getApp().globalData.host,
    curIndex: 0, //默认选中
    toView: 1,
    cat_id: 1, //初始化右侧列表
    pageNo: 1,
    catsType: [], //左侧导航
    pro_cat: [], //右侧列表,
    hasMore: true,
    load_done: false,
    isloadmain: false,
  },
  // 搜索
  to_search(e) {
    var that = this;

    if (e.detail.value != '') {
      this.setData({
        pageNo: 1,
        pro_cat: [], //右侧列表
        load_done: false
      })
      wx.showLoading();
      api.request('/shop/pro_cat', 'GET', {
        'page': that.data.pageNo,
        'row': 9,
        'cat_id': that.data.cat_id,
        'pro_name': e.detail.value
      }, (res) => {
        var pro_cat = that.data.pro_cat;
        if (res.data.data.length) {
          for (var i = 0; i < res.data.data.length; i++) {
            pro_cat.push(res.data.data[i]);
          }
        }
        that.setData({
          pro_cat: pro_cat,
          hasMore: res.data.has_more,
          isloadmain: true,
          load_done: true
        })
        wx.hideLoading()
      })
    } else {
      that.setData({
        pageNo: 1,
        pro_cat: [], //右侧列表
        load_done: false
      })
      that.load_typeList()
    }
  },
  // 选择左侧nav类别
  switchTab(e) {
    var that = this;
    that.setData({
      pageNo: 1, //重置页码
      pro_cat: [], //清空数组
      curIndex: e.currentTarget.dataset.index,
      cat_id: e.currentTarget.dataset.id,
      load_done: false
    })
    that.load_typeList()
  },

  // 跳转详情
  to_detail(e) {
    let pro_id = e.currentTarget.dataset.id;
    console.log(pro_id)
    wx.navigateTo({
      url: '../../detail/index/index?id=' + pro_id
    })
  },

  // 请求左侧导航分类
  load_nav() {
    var that = this;
    api.request('/shop/cats', 'GET', {}, (res) => {
      // console.log(res.data.data)
      let arr = res.data.data;
      arr.unshift(globalData.all_category)
      let id = arr[0].id
      that.setData({
        catsType: arr,
        cat_id: id
      })
      // 请求商品列表分类
      that.load_typeList()
    })
  },
  //请求商品分类列表
  load_typeList() {
    var that = this;
    if (that.data.pageNo == 1) {
      wx.showLoading()
    }
    var params = {} //发送的参数
    if (that.data.cat_id == 0) {
      params = {
        'page': that.data.pageNo,
        'row': 8
      }
    }else {
      params = {
        'page': that.data.pageNo,
        'row': 8,
        'cat_id': that.data.cat_id
      }
    }
    api.request('/shop/pro_cat', 'GET', params, (res) => {
      var pro_cat = that.data.pro_cat;
      if (res.data.data.length) {
        for (var i = 0; i < res.data.data.length; i++) {
          pro_cat.push(res.data.data[i]);
        }
      }
      that.setData({
        pro_cat: pro_cat,
        hasMore: res.data.has_more,
        isloadmain: true,
        load_done: true
      })
      wx.hideLoading()
    })
  },
  //滚动到底部
  onscrolltolower: function() {
    var that = this;
    that.data.pageNo++;
    if (that.data.hasMore) {
      that.load_typeList()
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.load_nav()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // 请求左侧导航分类
    let category = app.globalData.category;
    console.log(category.id)
    if (category.id) {
      this.setData({
        cat_id: category.id,
        curIndex: category.index,
        pageNo: 1, //重置页码
        pro_cat: [], //清空数组
      });
      this.load_typeList();
      app.globalData.category = {};
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})