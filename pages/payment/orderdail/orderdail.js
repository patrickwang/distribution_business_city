import api from '../../../utils/api'
import pay from '../../../utils/pay.js';
var app = getApp();
var globalData = app.globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    host: app.globalData.host,
    orderdetail: {},
    isloadmain: false
  },
  //获取订单详情
  _load_orderdetail() {
    var that = this;
    let value = wx.getStorageSync('order_no');
    api.request('/order/detail', 'GET', {
      'order_no': value
    }, (res) => {
      console.log(res.data.data)
      that.setData({
        orderdetail: res.data.data,
        isloadmain: true
      })
    });
    // wx.removeStorageSync('order_no')
  },

  // 去支付
  gotobuy: function(e) {
    let ind = e.currentTarget.dataset.ind
    let inx = e.currentTarget.dataset.inx
    // 支付
    if (inx === 1) { //去支付
      globalData.orderNo = ind;
      pay.generateOrder(function(){
        wx.navigateBack();
      });
    } else if (inx === 2 || inx === 5) { //申请退款
      wx.navigateTo({
        url: `/pages/parsonal/service/service?order_no=${ind}`,
      })
    } else if (inx === 3) {
      this.confirmReceipt(ind)
    }  else if (inx >= 6) { //查看售后进度
      wx.navigateTo({
        url: `/pages/parsonal/apply/index?order_no=${ind}`,
      })
    }
  },
  //申请退款
  applyRefund(e) {
    let that = this,
      status = e.currentTarget.dataset.inx,
      order = e.currentTarget.dataset.ind;
    if (status === 2) {
      api.request('/order/refund', 'POST', {
        order_no: order
      }, res => {
        if (res.data.status === 0) {
          this._load_orderdetail();
          wx.navigateTo({
            url: `/pages/parsonal/apply/index?indexorder_no=${order}`,
          })
        }
      })
    }
  },
  //确认收货
  confirmReceipt(order) {
    let that = this,
      order_no = order;
    wx.showModal({
      title: "提示",
      content: "是否确认收货",
      success: function(res) {
        if (res.confirm) {
          api.request(`/order/sign`, `POST`, {
            order_no: order_no
          }, (res) => {
            if (res.data.status == 0) {
              wx.showToast({
                title: '收货成功',
                icon: 'success',
                duration: 1000
              })
              wx.navigateBack()
            }
          })
        }
      }
    })
  },
  // 处理（取消、申请售后）订单
  handle_order(e) {
    var that = this;
    // 获取订单id
    let orderid = e.currentTarget.dataset.orderid;
    // 获取订单状态
    let inx = e.currentTarget.dataset.inx;
    console.log(orderid, inx)
    if (inx == 1) {
      wx.showModal({
        title: '提示',
        content: '确定要取消订单吗？',
        success: function(res) {
          if (res.confirm) {
            api.request('/order/cancel', 'POST', {
              order_no: orderid
            }, (res) => {
              if (res.data.status == 0) {
                wx.showToast({
                  title: '操作成功',
                  icon: 'success',
                  duration: 1000
                })
                setTimeout(() => {
                  wx.navigateBack()
                }, 1500)
              } else {
                wx.showToast({
                  title: '操作失败',
                  image: '/images/icon/baseclose.jpg',
                  duration: 2000
                })
              }

            });
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/parsonal/service/service?order_no=' + orderid
      })
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this._load_orderdetail();
    let that = this
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})