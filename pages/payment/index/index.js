// 确认订单
import api from '../../../utils/api';
import pay from '../../../utils/pay.js';
var app = getApp();
var globalData = app.globalData;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    host: app.globalData.host,
    isAddress: false,
    address: [],
    pay_text: '提交订单',
    // pro_arr:[],
    total: 0,
    total_pric: 0,
    total_freight: 0,
    sel_item: {},
    sel_price: 0,
    detail: [],
    has_ads: false, //是否有地址,
  },
  //跳转到收货地址
  to_address() {
    console.log(this.data.has_ads)
    if (this.data.has_ads) {
      wx.navigateTo({
        url: '../../parsonal/address/address',
      })
    } else {
      wx.navigateTo({
        url: '../../parsonal/editads/editads?type=add',
      })
    }

  },
  // 获取订单列表
  _get_detail() {
    var that = this;
    wx.getStorage({
      key: 'pro',
      success: function(res) {
        that.setData({
          detail: res.data
        })
        let total = 0;
        let total_freight = 0;
        let id_arr = [];
        let freight_arr = [];
        // 计算商品金额
        that.data.detail.forEach(function(item) {
          total += item.price * item.num
        })
        that.data.detail.forEach(function(item) {
          let id = item.freight_id;
          if (id_arr.indexOf(id) == -1) {
            id_arr.push(id);
            freight_arr.push(item.freight)
          }
        })
        // 计算运费
        freight_arr.forEach(function(item) {
          console.log(total)
          if (total >= item.ceiling_price) {
            total_freight: 0
          }
          else {
            total_freight += item.freight_price
          }
        })
        let tp = total + total_freight;
        that.setData({
          total: parseFloat(total).toFixed(2),
          total_freight: parseFloat(total_freight).toFixed(2),
          total_pric: parseFloat(tp).toFixed(2)
        })
      }
    })
  },

  // 获取收货地址
  _get_address() {
    var that = this;
    api.request('/address/list', 'GET', {}, (res) => {
      wx.hideLoading();
      if (res.data.data.length > 0) {
        let newaddress = res.data.data.filter((item) => {
          return item.default === 1
        })

        if (newaddress.length) {
          that.setData({
            address: newaddress
          })
        } else {
          that.setData({
            address: res.data.data
          })
        }

      } else {
        this.setData({
          address: []
        })
      }

    });

  },
  goaddaddress: function() {
    let pagecount = getCurrentPages().length;
    console.log(pagecount)
    if (pagecount >= 4 && !this.data.address.length) {
      wx.navigateTo({
        url: '/pages/parsonal/editads/editads?type=add'
      })
    } else {
      /**
       * @param  luo = sel 可以进行选择地址
       */
      wx.navigateTo({
        url: '/pages/parsonal/address/address?luo=sel'
      })
    }
  },
  // 提交订单
  sub_order() {
    if (!this.data.address.length) {
      wx.showToast({
        title: '请添加地址',
        image: '/images/icon/baseclose.jpg',
        duration: 2000
      })
      return
    }
    let pro_arr = [];
    let pro_item = {};
    this.data.detail.forEach(function(item) {
      pro_item = {
        'pro_id': item.id,
        'spec_id': item.spec_id,
        'count': item.num
      }
      pro_arr.push(pro_item)
    })
    console.log(pro_arr)
    wx.showToast({
      title: '订单创建中',
      icon: 'loading',
      duration: 10000,
      mask: true
    })
    // 下单
    api.request('/order/place', 'POST', {
      "addr_id": this.data.address[0].id,
      "products": pro_arr
    }, (res) => {
      wx.hideToast();
      globalData.orderNo = res.data.order_no
      pay.generateOrder(function() {
        let carts = wx.getStorageSync('carts');
        let pros = wx.getStorageSync('pro');
        console.log(globalData.paymentPage)
        //删除结算之后的购物车列表
        if (globalData.paymentPage == "cart") {
          for (let i = 0; i < pros.length; i++) {
            for (let j = 0; j < carts.length; j++) {
              if (pros[i].id === carts[j].id) {
                carts.splice(j, 1);
                j--;
              }
            }
          }
          wx.setStorageSync('carts', carts);
          wx.switchTab({
            url: '/pages/cart/index/index',
          })
        }else if (globalData.paymentPage == "detail") {
          wx.navigateBack();
        }
      
        // wx.switchTab({
        //   url: '/pages/cart/index/index',
        // })
      })
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // 获取订单列表
    this._get_detail()
    // //获取收货地址
    this._get_address()
    // 加载动画
    wx.showLoading({})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let val = wx.getStorageSync('seladdress')
    if (val) {
      this.setData({
        address: [val]
      })
      wx.removeStorageSync('seladdress')
    } else {
      this._get_address()
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})