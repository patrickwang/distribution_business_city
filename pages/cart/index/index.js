// pages/cart/index/index.js
var globalData = getApp().globalData;

Page({
  /**
   * 页面的初始数据
   */
  data: {
    host: getApp().globalData.host,
    cart0: false,
    selectAllStatus: false, //全选
    isSettle: false,
    totalPrice: 0, // 总价，初始为0
    carts: [] //购物车数据列表
  },

  //计算总价   总价 = 选中的商品1的 价格 * 数量 + 选中的商品2的 价格 * 数量 + ...
  getTotalPrice() {
    let carts = this.data.carts; // 获取购物车列表
    let total = 0,
      Allprice = 0;
    for (let i = 0; i < carts.length; i++) { // 循环列表得到每个数据
      if (carts[i].selected) { // 判断选中才会计算价格
        total += carts[i].num * carts[i].price; // 所有价格加起来
      }
    }
    //   //是否全选判断
    for (let i = 0; i < carts.length; i++) {
      // 合计价格
      Allprice = Allprice + carts[i].price * carts[i].num;
    }
    if (Allprice == total) {
      this.setData({
        selectAllStatus: true
      })
    } else {
      this.setData({
        selectAllStatus: false
      })
    }
    this.setData({ // 最后赋值到data中渲染到页面
      carts: carts,
      totalPrice: total.toFixed(2)
    });
  },

  //切换选择事件
  selectList(e) {
    var Allprice = 0,
      i = 0;
    const index = e.currentTarget.dataset.index; // 获取data- 传进来的index
    let carts = this.data.carts; // 获取购物车列表
    const selected = carts[index].selected; // 获取当前商品的选中状态
    carts[index].selected = !selected; // 改变状态
    this.setData({
      carts: carts
    });
    this.getTotalPrice(); // 重新获取总价
  },

  //全选
  selectAll(e) {
    let selectAllStatus = this.data.selectAllStatus; // 是否全选状态
    selectAllStatus = !selectAllStatus;
    let carts = this.data.carts;
    for (let i = 0; i < carts.length; i++) {
      carts[i].selected = selectAllStatus; // 改变所有商品状态
    }
    console.log(carts, !selectAllStatus);
    this.setData({
      selectAllStatus: selectAllStatus,
      carts: carts
    });
    this.getTotalPrice(); // 重新获取总价
  },

  // 增加数量
  addCount(e) {
    const index = e.currentTarget.dataset.index;
    let carts = this.data.carts;
    let num = carts[index].num;
    num = num + 1;
    carts[index].num = num;
    this.setData({
      carts: carts
    });
    this.getTotalPrice();
  },

  // 减少数量
  minusCount(e) {
    const index = e.currentTarget.dataset.index;
    let carts = this.data.carts;
    let num = carts[index].num;
    if (num <= 1) {
      return false;
    }
    num = num - 1;
    carts[index].num = num;
    this.setData({
      carts: carts
    });
    this.getTotalPrice();
  },

  //随便逛逛
  casual() {
    wx.switchTab({
      url: '../../home/index/index'
    })
  },

  // 删除
  deleItem(e) {
    console.log(e.currentTarget.dataset.indx);
    // this.modal.hideModal()
    var that = this;

    wx.showModal({
      title: '提示',
      content: '是否删除该商品？',
      success: function(res) {
        if (res.confirm) {
          let indx = e.currentTarget.dataset.indx;
          let carts = that.data.carts;
          let total = that.data.totalPrice;
          if (carts[indx].selected == true) {
            total -= carts[indx].num * carts[indx].price;
          }

          // 删除指定下标元素
          carts.splice(indx, 1);
          that.setData({
            carts: carts,
            totalPrice: total
          })
          if (carts.length == 0) {
            that.setData({
              cart0: true,
              selectAllStatus: false,
              totalPrice: 0
            })
          }
          wx.setStorageSync('carts', that.data.carts);
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },
  cancelDelect() {
    console.log("你点击了取消")
  },
  confirmDelect() {
    console.log("你点击了确定")
  },

  // 提交订单
  sub_payment() {
    let arr = this.data.carts.filter(function(item) {
      return item.selected == true
    })

    console.log(arr)
    wx.setStorage({
      key: 'pro',
      data: arr
    })

    if (arr.length != 0) {
      globalData.paymentPage = "cart"; 
      wx.navigateTo({
        url: '/pages/payment/index/index',
      })
    } else {
      wx.showToast({
        title: '请勾选商品',
        image: '/images/icon/baseclose.jpg',
        duration: 2000
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function() {
    console.log(1)
    this.setData({
      totalPrice: this.data.totalPrice.toFixed(2)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.modal = this.selectComponent("#modal")
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let carts = wx.getStorageSync('carts');
    let isCart0 = carts.length == 0 ? true: false;
    this.setData({
      carts: carts,
      cart0: isCart0,
      totalPrice: 0.00,
      selectAllStatus: false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})