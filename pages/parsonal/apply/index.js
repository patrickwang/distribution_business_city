// pages/parsonal/apply/index.js
import api from "../../../utils/api.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    order: {}
  },
  getOrderData(params) {
    let that = this;
    api.request('/order/detail', 'GET', params, res => {

      console.log(res.data)
      this.setData({
        order: res.data.data
      })

    })
  },
  sub_express(params) {
    api.request('/order/detail', 'POST', params, res => {
      if (res.data.status == 0) {
        wx.navigateTo({
          url: '../express/index',
        })
      } else {
        wx.showModal({
          title: '提示',
          content: res.data.msg
        })
      }
    })
    wx.navigateTo({
      url: '../express/index',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getOrderData(options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})