// 立即评价
import api from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    evalid:-1,//评价的订单id,
    host: getApp().globalData.host,
    listData: [{
      img:'',
      starcount:5,
      staractive:5
    }],
    listOther: {}
  },
  /**
   * 星星的选择
  */
  optStar: function (e) {
    let inz = e.currentTarget.dataset.inz
    let st = "listData[" + inz + "].staractive"
    this.setData({
      [st]: e.currentTarget.dataset.inx
    })
  },

  /**
 * 字数计算
*/
  wordpublish: function (e) {
    let inz = e.currentTarget.dataset.inz
    let st = "listData[" + inz + "].wordcount"
    let sb = "listData[" + inz + "].content"
    this.setData({
      [st]: e.detail.value.length,
      [sb]: e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      evalid: options.orderid
    })
    let val = wx.getStorageSync('evalitems')
    console.log(val)
    let arr = [];
    for(let i = 0;i < val.length;i++){
      arr[i] = {
        pro_id:val[i].pro_id,
        starcount: 5,
        staractive: 5,
        img: val[i].pro_img
      }
    }
      this.setData({
        listData: arr
      })
  },

  publishbtn: function () {
    console.log(this.data.listData)
    let ary = this.data.listData

    let comary = []
    for (let i = 0; i < ary.length; i++) {
      comary[i] = {
        pro_id: ary[i].pro_id,
        score: ary[i].staractive,
        content: ary[i].content
      }
    }

    const data = {
      order_no: this.data.evalid,
      comments: comary
    }

    wx.showToast({
      title: '商品评论中...',
      icon: 'loading',
      duration: 10000,
      mask: true
    })
    api.request('/comment/save', 'POST', data, (res) => {
      wx.showToast({
        title: '评论成功',
        icon: 'success',
        duration: 1000
      })
      setTimeout(() => {
        wx.navigateBack()
      }, 1500)
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})