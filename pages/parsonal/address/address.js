// 我的地址
import api from '../../../utils/api'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    address_list:[],
    isloadmain:false,
    active_idx:0,
    selfun: false    // 是否可以选择的函数开启
  },

  // 添加收货地址
  add_ads(){
    wx.navigateTo({
      url:'../editads/editads?type=add'
    })
  },
  
  // 获取地址列表
  _getAds_list(){
    api.request('/address/list', 'GET', {}, (res) => {
      this.setData({
        address_list: res.data.data,
        isloadmain:true
      })
    });
  },

  // 编辑
  to_edit(e) {
    var that = this
    let item= e.currentTarget.dataset.item;
    let id= item.id;
    wx.showActionSheet({
      itemList: ['编辑', '删除'],
      success: function (res) {
        if(res.tapIndex == 0){
          //编辑
          wx.setStorage({
            key: "edit_item",
            data: item
          })
          wx.navigateTo({
            url: '../editads/editads?type=edit',
          })
        } else if (res.tapIndex == 1){
          // 删除
          wx.showModal({
            title: '提示',
            content: '是否删除？',
            success: function (res) {
              if (res.confirm) {
                api.request('/address/delete', 'POST', {
                  'id': id
                }, (res) => {
                  wx.showToast({
                    title: '删除成功',
                    icon: 'success',
                    duration: 2000
                  })
                  // 刷新地址列表
                  that._getAds_list()
                })
              } else {
              }
            }
          })
        }
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },

  // 选择收货地址
  choose_ads(e){
    if (this.data.selfun) {
      let inx = e.currentTarget.dataset.inx
      wx.setStorageSync("seladdress", e.currentTarget.dataset.ads)
      wx.navigateBack()
    }
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.luo === 'sel') {
      this.setData({
        selfun: true
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this._getAds_list();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})