// pages/parsonal/income/index.js
import api from '../../../utils/api.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    incomeData: {}
  },
  //获取收入信息
  getIndexData () {
    let that = this;
    api.request('/user/income','GET',{},res => {
      if(res.data.status == 0) {
        let incomeData = res.data.data;
        console.log(res.data.data)
        that.setData({
          incomeData
        })
      }
    })
  },
  //申请提现
  apply () {
    let that = this;
    api.request('/user/withdraw','POST',{},res => {
      if ( res.data.status == 0 ) {
        
      }
      wx.showModal({
        title: '提示',
        content: res.data.msg,
        success () {
          that.getIndexData(); 
        }
      })
    })
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取收入信息
    this.getIndexData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})