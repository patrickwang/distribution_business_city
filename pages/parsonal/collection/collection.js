Page({

  /**
   * 页面的初始数据
   */
  data: {
    host:getApp().globalData.host,
    list:[]
  },

  // 获取收藏列表
  _load_collect(){
    var that = this;
    let arr = wx.getStorageSync('collect_arr');
    if(arr.length == 0){
      that.setData({
        list:[]
      })
    }else{
      that.setData({
        list: arr
      })
    }
  },
  
  //随便逛逛
  casual() {
    wx.switchTab({
      url: '../../home/index/index'
    })
  },

  // 取消收藏
  cal_col(e){
    console.log(e.currentTarget.dataset.indx);
    var that = this;
    wx.showModal({
      title: '提示',
      content: '是否取消收藏该商品？',
      success: function (res) {
        if (res.confirm) {
          let indx = e.currentTarget.dataset.indx;
          let list = that.data.list;
          // 删除指定下标元素
          list.splice(indx, 1);
          that.setData({
            list: list
          })
          if (list.length == 0) {
            // that.setData({
            //   cart0: true
            // })
          }
          wx.setStorageSync('collect_arr', that.data.list);
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 跳转到详情页面
  to_detail(e){
    wx.navigateTo({
      url: '../../detail/index/index?id=' + e.currentTarget.dataset.id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this._load_collect()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})