
import { Base } from '../../../utils/base.js'

class Api extends Base {
  constructor() {
    super()
  }

  // 获取用户信息
  getUserInfo(cb) {
    let that = this
    wx.login({
      success() {
        wx.getUserInfo({
          success(res) {
            typeof cb === 'function' && cb(res.userInfo)
          },
          fail() {
            typeof cb === 'function' && cb({
              avatarUrl: '../../../images/icon/user@default.png',
              nickName: '微友货'
            })
          }
        })
      }
    })
  }
}

export { Api }