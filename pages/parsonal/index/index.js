/**
 * 个人中心
 */
import api from '../../../utils/api'
import {
  Api
} from './index-model.js'

var appi = new Api()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    num1: 0,
    num2: 0,
    num3: 0,
    num4: 0,
    num6: 0,
    scene: {}, //是否有二维码
    isSignIn: false,
    personData: {}, //用户信息
    userInfo: {}, //用户信息
    order_items: [{
        id: 0,
        img: "/images/icon/personal_order_img1.png",
        text: "待付款"
      },
      {
        id: 1,
        img: "/images/icon/personal_order_img2.png",
        text: "待发货"
      },
      {
        id: 2,
        img: "/images/icon/personal_order_img3.png",
        text: "已发货"
      },
      {
        id: 3,
        img: "/images/icon/personal_order_img4.png",
        text: "退款/售后"
      },
    ]
  },
  // 跳转到我的订单页面
  to_order(event) {
    var status = event.currentTarget.dataset.status;
    var that = this;
    console.log(typeof status)
    var orderStatus = [1, 2, 3, 6];
    if (status < 4) {
      wx.navigateTo({
        url: '../order/order?status=' + orderStatus[status]
      })
    } else if (status == 4) {
      //如果是一级分销商
      if (that.data.userInfo.level === 1) {
        wx.navigateTo({
          url: '../income/index'
        })
      } //如果是客户 
      else {
        wx.navigateTo({
          url: '../order/order?status=0'
        })
      }
    } else if (status == 5) {
      //如果是客户
      if (that.data.userInfo.level === 0) {
        wx.navigateTo({
          url: '../address/address',
        })
      } else {
        wx.navigateTo({
          url: '../client/index'
        })
      }
    }
  },
  // 邀请分享
  to_share() {
    if (this.data.haveToken) {
      wx.navigateTo({
        url: '/pages/share/index?page=pages/parsonal/index/index',
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '请登录之后再进行操作',
        showCancel: false
      })
    }
  },
  _login: function() {
    let that = this;
    wx.login({
      success: function(ret) {
        var code = ret.code;
        var data = {
          "code": code
        };
        api.request('/user/login_code', 'POST', data, (res) => {
          wx.setStorageSync('USER_TOKEN', res.data.data.token);
          that.getUserInfo();
        });
      }
    })
  },


  //token校验
  _checkToken() {
    var that = this;
    var userToken = wx.getStorageSync('USER_TOKEN');
    api.request('/user/check', 'POST', {
      token: userToken
    }, (res) => {
      console.log(res.data.check);
      var checkStatus = res.data.check;
      //如果校验失败
      if (!checkStatus) {
        that._login();
        console.log('校验失败');
      } else {
        that.getUserInfo();
      }
    }, (res) => {
      wx.showModal({
        title: '错误',
        content: '服务器检测token错误',
      })
    });
  },

  _autoLogin: function() {
    var userInfo = wx.getStorageSync('USER_INFO');
    var friend_id = this.data.scene.user_id;
    if (!userInfo) {
      //第一次进入小程序
      wx.hideLoading();
      this.setData({
        haveToken: false
      })
      // 如果有二维码参数则提示登录
      if (friend_id) {
        wx.showModal({
          title: '提示',
          content: '请先登录',
        })
      }
    } else {
      //token校验
      this._checkToken();
      this.setData({
        haveToken: true
      })
    }
  },
  //获取微信授权
  bindGetUserInfo: function(e) {
    if (e.detail.errMsg == "getUserInfo:ok") {
      let that = this;
      let friend_id = this.data.scene.user_id;
      let pro_id = this.data.scene.product_id;
      wx.showLoading({
        title: '登录中',
      })
      if (e.detail.userInfo) {
        wx.login({
          success(ret) {
            let code = ret.code,
              uDetail = e.detail,
              data = {
                code: code,
                encryptedData: uDetail.encryptedData,
                iv: uDetail.iv
              };
            api.request('/user/decrypt', 'POST', data, (res) => {
              wx.setStorageSync('USER_INFO', res.data.data);
              wx.setStorageSync('USER_TOKEN', res.data.data.token);
              wx.hideLoading();
              that.getUserInfo();
              that.setData({
                haveToken: true,
              })

            }, () => {
              console.log("接口调用失败");
              wx.hideLoading();
              wx.showModal({
                title: '提示',
                content: '登录失败，请重新登录',
              })
            });
          }
        });
      } else {

      }
    }
  },

  /**
   * 获取用户数据
   */
  getUserInfo() {
    let that = this;
    let friend_id = this.data.scene.user_id; //二维码进入的关联的userid
    api.request('/user/info', 'GET', {}, (res) => {
      if (res.data.status === 0) {
        //如果用户信息被删除
        if (res.data.user_info == null) {
          wx.removeStorageSync("USER_INFO");
          wx.removeStorageSync("USER_TOKEN");
          that.setData({
            haveToken: false
          })
        }

        wx.setStorageSync('USER_INFO', res.data.user_info)
        that.setData({
          userInfo: res.data.user_info,
        })

        that.getPersonData();
        //如果通过二维码进来则建立联系
        if (friend_id) {
          that.buildRelationship(friend_id)
        }
      } else {
        wx.showModal({
          content: res.data.meg,
        })
      }
      wx.hideLoading();
    })
  },
  //建立联系
  buildRelationship(id) {
    let product_id = this.data.scene.product_id //是否是产品页面跳转过来的
    let that = this;
    api.request('/user/relation', 'POST', {
      friend_id: id
    }, res => {
      if (res.data.status == 0) {
        wx.removeStorageSync("scene");
        that.getUserInfo();
        if (product_id != "0") {
          wx.navigateTo({
            url: `/pages/detail/index/index?id=${product_id}`,
          })
        }
        // wx.showModal({
        //   title: '提示',
        //   content: '建立关系成功',
        //   success() {
        //     if (product_id != "0") {
        //       wx.navigateTo({
        //         url: `/pages/detail/index/index?id=${product_id}`,
        //       })
        //     }
        //     this.getPersonData();
        //   }
        // })
      } else {
        wx.removeStorageSync("scene");
        // wx.showModal({
        //   title: '提示',
        //   content: res.data.msg
        // })
      }
    })
  },
  //获取个人中心数据
  getPersonData() {
    let that = this
    api.request('/user/index', 'POST', {}, res => {
      if (res.data.status == 0) {
        that.setData({
          personData: res.data.user_info
        })
      }
    })
  },
  //字符串转换对象
  parseQueryString(url) {
    let items = url.split("&"); //分割成数组
    let params = {};
    let arr, name, value;
    for (var i = 0; i < items.length; i++) {
      arr = items[i].split("="); //["key0", "0"]
      name = arr[0];
      value = arr[1];
      params[name] = value;
    }
    return params
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.showLoading({
      title: '加载中',
    })
    let scene = options.scene == undefined ? wx.getStorageSync("scene") : decodeURIComponent(options.scene); //获取二维码参数
    wx.setStorageSync("scene", scene);

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let scene = wx.getStorageSync("scene");
    let scene2 = this.parseQueryString(scene);
    this.setData({
      scene: scene2
    })
    this._autoLogin();

  },


  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }

})