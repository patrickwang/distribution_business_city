// 编辑、新增收货地址
import api from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    region: ['省份', '城市', '区县'],
    edit_item:{},
    province:'',
    city:'',
    town:'',
    type:'',
    ischecked: true,    // 是否选中为默认地址
    isChange: false
  },
  // 修改省市
  bindRegionChange: function (e) {
    this.setData({
      region: e.detail.value,
      isChange: true
    })
  },
  // 选择是否为默认地址
  switch1Change: function () {
    this.data.ischecked = !this.data.ischecked;
    console.log(this.data.ischecked)
    this.setData({
      ischecked:this.data.ischecked
    })
  },
  //提示
  _showModal(msg) {
    wx.showToast({
      title: msg,
      image: '/images/icon/baseclose.jpg',
      duration: 2000
    })
  },
  // 保存
  formSubmit: function (e) {
    var that = this;
    let formData = e.detail.value;
    formData.default = that.data.ischecked === true ? '1' : '0';
    let ads = this.data.region;
    let province = ads[0];//省
    let city = ads[1];//市
    let town = ads[2];//县、区
    formData['province'] = province;
    formData['city'] = city;
    formData['town'] = town;
    console.log('form携带数据为：', formData);
    
    // 表单验证
    if (formData.name == ''){
      that._showModal('请输入姓名');
      return;
    }
    // var test_phone = /^1[34578]\d{9}$/.test(formData.phone);
    var test_phone = /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/.test(formData.phone)
    if (formData.phone == '' || !test_phone) {
      that._showModal('请填正确手机号');
      return;
    }
    if (formData.province=='省份' || formData.city=='城市' || formData.town=='区县') {
      that._showModal('请输入地址')
      return;
    }
    if (formData.address == ''){
      that._showModal('请输入详细地址');
      return;
    }
    console.log(formData)

    // console.log(formData)
    if(that.data.type == 'add'){
      // 添加地址
      api.request('/address/save?XDEBUG_SESSION_START=PHPSTORM', 'POST', formData, (res) => {
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
        setTimeout(function () {
          wx.navigateBack()
        }, 1500)
      });
    } else if (that.data.type == 'edit'){
      // 编辑地址
      formData.id = that.data.edit_item.id;
      api.request('/address/save?XDEBUG_SESSION_START=PHPSTORM', 'POST', formData, (res) => {
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
        setTimeout(function(){
          wx.navigateBack()
        },1500)
      });
    }
    
  },
  //取消
  cancel: function() {
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var opt = options.type;
    var that = this;
    if(opt == 'add'){
      wx.setNavigationBarTitle({
        title: '新增地址'
      })
      that.setData({
        type:'add'
      })
    }else if(opt == 'edit'){
      wx.setNavigationBarTitle({
        title: '修改地址'
      })
      that.setData({
        type: 'edit'
      })
      // 获取修改的地址
      wx.getStorage({
        key: 'edit_item',
        success: function (res) {
          let region = [res.data.province, res.data.city, res.data.town];
          that.setData({
            edit_item: res.data,
            region: region
          })
        }
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})