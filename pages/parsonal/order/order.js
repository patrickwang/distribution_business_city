// 我的订单
import api from '../../../utils/api'
import pay from '../../../utils/pay.js';
var app = getApp();
Page({
  data: {
    host: getApp().globalData.host,
    tabs: ["全部", "待付款", "待发货", "已发货", "退款/售后"],
    listData: [],   // 产品数据
    hasMore: true,
    load_done: false,
    activeIndex: -1,
    items: [],
    pageNo: 1,
    statusCode: 0,//订单状态
    order0: false,
    scroll_top: 0,
    winHeight: '',
    userInfo: {}
  },

  //获取订单列表
  _load_orderlist() {
    var that = this;
    if (that.data.pageNo == 1) {
      wx.showLoading()
    }
    api.request('/order/list', 'GET', {
      page: that.data.pageNo,
      row: 5,
      status: that.data.statusCode
    }, (res) => {
      console.log(res.data)
      wx.hideLoading();
      that.setData({
        load_done: true
      })
      // 判断是否有订单
      if (that.data.pageNo === 1 && !res.data.data.length) {
        that.setData({
          order0: true
        })
      }
      var listData = that.data.listData;
      if (res.data.data.length) {
        for (var i = 0; i < res.data.data.length; i++) {
          listData.push(res.data.data[i]);
        }
      }
      that.setData({
        listData: listData,
        hasMore: res.data.has_more//判断是否有更多
      })
      wx.hideLoading()
    });
  },

  //滚动到底部
  onscrolltolower: function () {
    var that = this;
    that.data.pageNo++;
    if (that.data.hasMore) {
      that._load_orderlist()
    }
  },

  // 去支付
  gotobuy: function (e) {
    let ind = e.currentTarget.dataset.ind
    let inx = e.currentTarget.dataset.inx
    // 支付
    if (inx === 1) {
      wx.setStorageSync('orderid', ind)
      pay.generateOrder()
      console.log(ind)
    }
  },

  // 去评价
  to_eval(e) {
    let orderid = e.currentTarget.dataset.orderid;
    let evalitems = e.currentTarget.dataset.items;
    wx.setStorageSync('evalitems', evalitems)
    wx.navigateTo({
      url: '/pages/parsonal/evaluate/evaluate?orderid=' + orderid
    })
  },

  // 申请售后
  to_sh(e) {
    let orderid = e.currentTarget.dataset.orderid;
    wx.navigateTo({
      url: '/pages/parsonal/service/service?order_no=' + orderid
    })
  },

  onLoad: function (option) {
    let that = this;
    let userInfo = wx.getStorageSync("USER_INFO");
    let status = option.status;
    wx.showLoading()
    // 判断是初始化时加载的订单类型
    that.setData({
      statusCode: status,
      listData: [],
      userInfo: userInfo
    })
    if (status == 4) {
      that.setData({
        activeIndex: 3
      })
    } else if (status == 6) {
      that.setData({
        activeIndex: 4
      })
    } else {
      that.setData({
        activeIndex: status
      })
    }
    //  高度自适应
    wx.getSystemInfo({
      success: function (res) {
        var clientHeight = res.windowHeight,
          clientWidth = res.windowWidth,
          rpxR = 750 / clientWidth;
        var calc = clientHeight * rpxR - 180;
        console.log(calc)
        that.setData({
          winHeight: calc
        });
      }
    });

  },

  // 选择订单类型
  tabClick: function (e) {
    // let item = e.currentTarget.dataset.item;
    // let statusArray = [0, 1, 2, 3, 6] //订单状态码数组
    this.setData({
      activeIndex: e.currentTarget.dataset.index,
      load_done: false,
      listData: [],//清空列表
      pageNo: 1,
      scroll_top: 0,
      order0: false
    })
    // let indx = this.data.activeIndex;
    // this.setData({
    //   statusCode: statusArray[indx],
    // })
    // 加载订单列表
    // this._load_orderlist()
  },
  //滑动切换导航
  switchTab(e) {
    let item = e.detail.current;
    let statusArray = [0, 1, 2, 3, 6] //订单状态码数组
    this.setData({
      activeIndex: item,
      load_done: false,
      listData: [],//清空列表
      pageNo: 1,
      scroll_top: 0,
      order0: false
    })
    let indx = this.data.activeIndex;
    this.setData({
      statusCode: statusArray[indx],
    })
    // 加载订单列表
    this._load_orderlist()
  },

  // 查看订单详情
  to_orderdetail(e) {
    let orderid = e.currentTarget.dataset.orderid;
    let level = this.data.userInfo.level;

    if(level>0) {
      return ;
    }
    wx.setStorageSync('order_no', orderid);
    // 跳转到详情页面
    wx.navigateTo({
      url: '/pages/payment/orderdail/orderdail'
    })
  },

  // 点击取消订单
  cal_order(e) {
    var that = this;
    // 获取订单id
    let orderid = e.currentTarget.dataset.orderid;
    wx.showModal({
      title: '提示',
      content: '确定要取消订单吗？',
      success: function (res) {
        if (res.confirm) {
          api.request('/order/cancel', 'POST', {
            order_no: orderid
          }, (res) => {
            wx.showLoading({
              title: '正在取消订单...'
            })
            if (res.data.status == 0) {
              wx.hideLoading()
              setTimeout(function () {
                wx.showToast({
                  title: '操作成功',
                  icon: 'success',
                  duration: 2000
                })
              }, 500)
              that.setData({
                pageNo: 1,
                listData: []
              })
              // 刷新列表
              that._load_orderlist()
            } else {
              wx.showToast({
                title: '操作失败',
                image: '/images/icon/baseclose.jpg',
                duration: 2000
              })
            }

          });
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  //订单签收
  receipt(e) {
    var that = this;
    let orderid = e.currentTarget.dataset.orderid;
    wx.showModal({
      title: '提示',
      content: '是否确认收货？',
      success: function (res) {
        if (res.confirm) {
          api.request('/order/sign', 'POST', {
            order_no: orderid
          }, (res) => {
            wx.showToast({
              title: '收货成功',
              icon: 'success',
              duration: 1000
            })
            that.setData({
              pageNo: 1,
              listData: []
            })
            // 刷新列表
            that._load_orderlist()
          })
        }
      }
    })
  },
  onShow() {
    var that = this;
    that.setData({
      load_done: false,
      listData: [],//清空列表
      pageNo: 1,
      scroll_top: 0,
      order0: false
    })
    that._load_orderlist()

  }
});