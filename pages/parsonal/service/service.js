import api from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    order_no:''
  },

  // 获取售后原因
  custominput: function (e) {
    this.setData({
      customtxt: e.detail.value
    })
  },

  // 提交售后信息
  post_info(){
    if (!this.data.customtxt) {
      wx.showToast({
        title: '请输入退换货原因',
        image: '/pages/images/icon/baseclose.jpg',
        duration: 2000
      })
      return
    }
    wx.showToast({
      title: '售后服务申请中...',
      icon: 'loading',
      duration: 10000,
      mask: true
    })
    api.request('/order/refund', 'POST', {
      order_no: this.data.order_no,
      remarks: this.data.customtxt
    }, (res) => {
      wx.showToast({
        title: '申请成功',
        icon: 'success',
        duration: 1000
      })
      setTimeout(() => {
        wx.navigateTo({
          url: '../order/order?status=6',
        })
      }, 1500)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.order_no);
    this.setData({
      order_no: options.order_no
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})