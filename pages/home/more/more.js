// 今日推荐列表

import api from '../../../utils/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    host: getApp().globalData.host,
    pageNo:1,
    list:[],
    hasMore:true
  },
  //获取今日推荐列表
  _load_recom(){
    var that = this;
    api.request('/shop/pro_top', 'GET', {
      page:that.data.pageNo,
      row:10
    }, (res) => {
      var list = that.data.list;
      if (res.statusCode == 200) {
        if (res.data.data.length) {
          for (var i = 0; i < res.data.data.length; i++) {
            list.push(res.data.data[i]);
          }
        }
        that.setData({
          list: list,
          hasMore: res.data.has_more//判断是否有更多
        })
      }
    })
  },
  //滚动到底部
  onscrolltolower: function () {
    var that = this;
    that.data.pageNo++;
    if (that.data.hasMore) {
      that._load_recom()
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._load_recom()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})