// 主页

import api from '../../../utils/api'

var app = getApp();
var globalData = app.globalData
Page({


  /**
   * 页面的初始数据
   */
  data: {
    host: app.globalData.host,
    isloadmain: false,
    index:{},//首页数据
    banners:[],//轮播图
    prods_month:[],//本月上新
    categoryList: [],// 分类
    themes: [],
    recomme:[],
    hasMore: true,
    pageNo: 1,
    list: [],
  },

  //分类跳转
  to_category(e) {
    let id = e.currentTarget.dataset.id, 
        index = e.currentTarget.dataset.index
    app.globalData.category = {
      id,
      index
    };
    wx.switchTab({
      url: `/pages/category/index/index`
    })
  },

  //主题跳转
  to_list(e){
    var group_id = e.currentTarget.dataset.id;
    var title = e.currentTarget.dataset.title;
    var img = e.currentTarget.dataset.img;
    wx.navigateTo({
      url: '../../list/index/index?id=' + group_id + '&title=' + title + '&img=' + img,
    })
  },
  //请求首页数据
  _load_data(){
    let that = this;
    api.request("/shop/index", "GET",{},(res) => {
      let index = res.data
      index.prods_hot.data = index.prods_hot.data.slice(0,4);
      index.prods_recent.data = index.prods_recent.data.slice(0,2)
      console.log(index.sliders.data)
      that.setData({
        banners: index.sliders.data,
        index: index,
        isloadmain: true
      })
    })
  },


  // 点击轮播图
  listlook(e){
    console.log(e.currentTarget.dataset.id)
    console.log(e.currentTarget.dataset.type)
    let type = e.currentTarget.dataset.type;
    let type_id = e.currentTarget.dataset.id;
    let title = e.currentTarget.dataset.title;
    let img = e.currentTarget.dataset.img;
    if (type == 'detail'){
      // 跳转到详情页面
      wx.navigateTo({
        url: '../../detail/index/index?id=' + type_id
      })
    } else if (type == 'group'){
      // 跳转到专题页面
      wx.navigateTo({
        url: '../../list/index/index?id=' + type_id + '&title=' + title + '&img=' + img,
      })
    }else{}
  },
  //请求首页分类
  _load_category(){
    let that = this;
    api.request("/shop/cats","GET", {},
    (res) => {
      if(res.data.status ===0) {
        that.setData({
          categoryList:res.data.data
        })
      }
    }
    )
  },
  //请求今日推荐
  _load_recom(){
    var that = this;
    api.request('/shop/pro_top', 'GET', {
      page:1,
      row:5
    }, (res) => {
      that.setData({
        recomme: res.data.data,
        hasMore: res.data.has_more
      })
    })
  },

  //请求商品专题
  _load_theme(){
    var that = this;
    api.request('/shop/groups', 'GET', {}, (res) => {
      that.setData({
        themes: res.data.data,
        isloadmain: true
      })
    })
  },

  //查看今日推荐详情
  to_detail(e){
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../../detail/index/index?id=' + id
    })
  },

  // 请求全部商品列表
  _load_all(){
    var that = this;
    api.request('/shop/pro_cat', 'GET', {
      page: that.data.pageNo,
      row: 10,
    }, (res) => {
      var list = that.data.list;
        if (res.data.data.length) {
          for (var i = 0; i < res.data.data.length; i++) {
            list.push(res.data.data[i]);
          }
        }
        that.setData({
          list: list,
          hasMore: res.data.has_more//判断是否有更多
      
        })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
  },
  
  //页面滚到底部
  onscrolltolower(){
    var that = this;
    that.data.pageNo++;
    if (that.data.hasMore) {
      that._load_all()
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 请求首页分类、
    this._load_category()
    // // 请求今日推荐
    // this._load_recom()
    // // 请求主题
    // this._load_theme()
    // // 请求全部商品
    // this._load_all()
    // 请求首页数据
    this._load_data()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})