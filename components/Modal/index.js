// components/Modal/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    content: {
      type: String
    },
    cancel: {
      type: String
    },
    confirm: {
      type: String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShow: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 隐藏显示模态框
    hideModal() {
      this.setData({
        isShow: !this.data.isShow
      })
    },
    // 点击取消触发回调
    _cancelEvent(){
      this.triggerEvent("cancelEvent")
    },
    // 点击确定触发回调
    _confirmEvent() {
      this.triggerEvent("confirmEvent")
    }
  }
})
