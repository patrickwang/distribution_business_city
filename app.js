//app.js
App({
  /**
   * 全局用户信息变量
   */
  globalData: {
    APPID: 'wx407e41c34efadd2f',
    // appletId: '47cbcc3aef82744ba6ec00737ed1c8a7',//微友货
    appletId: "iskdnv56c8e909daeeb763317fe5ee2a", //分销商城
    host: 'https://fx.huaict.com',
    category: "", //当前点击分类选项
    orderNo: "", //下单的订单号
    page: "",
    paymentPage: "", //付款页面是从哪里跳转过来的。
    all_category: {
      'cat_name': '全部',
      'cat_image': '/images/icon/project_man1.png',
      'id': 0
    }
  },
  onLaunch: function() {

  },
  onShow: function() {

    var that = this;
  },

})



// 展示本地存储能力
// var logs = wx.getStorageSync('logs') || []
// logs.unshift(Date.now())
// wx.setStorageSync('logs', logs)

// // 登录
// wx.login({
//   success: res => {
//     // 发送 res.code 到后台换取 openId, sessionKey, unionId
//   }
// })
// // 获取用户信息
// wx.getSetting({
//   success: res => {
//     if (res.authSetting['scope.userInfo']) {
//       // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
//       wx.getUserInfo({
//         success: res => {
//           // 可以将 res 发送给后台解码出 unionId
//           this.globalData.userInfo = res.userInfo

//           // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
//           // 所以此处加入 callback 以防止这种情况
//           if (this.userInfoReadyCallback) {
//             this.userInfoReadyCallback(res)
//           }
//         }
//       })
//     }
//   }
// })