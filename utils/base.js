
import { Config } from 'config.js'

class Base {
  constructor() {
    this.baseRestUrl = Config.restUrl
  }

  /**
   * http 接口请求
   * @param params 多量参数
   */
  request(params,callback) {
    let that = this
    let url = this.baseRestUrl + params.url
    if (!params.type) {
      params.type = 'GET'
    }
    wx.request({
      url: url,
      data: params.data,
      method: params.type,
      header: {
        'content-type': 'application/json',
        'token': wx.getStorageSync('token'),
        'appletId':'47cbcc3aef82744ba6ec00737ed1c8a7'
      },
      success(res) {
        // 状态码
        if(res.statusCode == 200){
          callback(res)
        }
      },
      fail(res) {
        console.log(res)
      }
    })
  }
}

export { Base }
