// const formatTime = date => {
//   const year = date.getFullYear()
//   const month = date.getMonth() + 1
//   const day = date.getDate()
//   const hour = date.getHours()
//   const minute = date.getMinutes()
//   const second = date.getSeconds()

//   return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
// }

// const formatNumber = n => {
//   n = n.toString()
//   return n[1] ? n : '0' + n
// }

// module.exports = {
//   formatTime: formatTime
// }
const formatDate = (date, fmt) => {
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
  }
  let o = {
    'M+': date.getMonth() + 1,
    'd+': date.getDate(),
    'h+': date.getHours(),
    'm+': date.getMinutes(),
    's+': date.getSeconds()
  }
  for (let k in o) {
    if (new RegExp(`(${k})`).test(fmt)) {
      let str = o[k] + ''
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str))
    }
  }
  return fmt
}

function padLeftZero(str) {
  return ('00' + str).substr(str.length)
}


const addDateTime = (days) => {
  if (days == undefined || days == '') {
    days = 1
  }
  let date = new Date()
  date.setDate(date.getDate() + days)
  let month = date.getMonth() + 1
  let day = date.getDate()
  let mm = "'" + month + "'"
  let dd = "'" + day + "'"

  //单位数前面加0
  if (mm.length == 3) {
    month = "0" + month
  }
  if (dd.length == 3) {
    day = "0" + day
  }

  let time = date.getFullYear() + "-" + month + "-" + day
  return time
}

/* 日期比较 */
const CompareDate = (d1, d2) => {
  return ((new Date(d1.replace(/-/g, "\/"))) >= (new Date(d2.replace(/-/g, "\/"))));
}

module.exports = {
  formatDate: formatDate,
  addDateTime: addDateTime,
  CompareDate: CompareDate
}

