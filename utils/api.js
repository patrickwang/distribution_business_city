var app = getApp();
let request = (_url, _type, _data, callback,failcallback) => {
  // 获取本地保存的信息 
  var user_token = wx.getStorageSync('USER_TOKEN');
  wx.request({
    url: app.globalData.host + '/api' + _url,
    data: _data,
    method: _type,
    header: {
      'content-type': 'application/json',
      'appletId': app.globalData.appletId,
      'token': user_token
    },
    success: ((res) => {
      if(res.data.status == -2){
        _login();
      }
      if (res.statusCode == 200 || res.statusCode == 201) {
        callback(res)
      }
      else {
        failcallback()
      }
    }),
    fail: ((res) => {
      failcallback(res)
    })
  })
}

function _login() {
  wx.login({
    success: function (ret) {
      var code = ret.code;
      var data = { "code": code };
      api.request('/user/login_code', 'POST', data, (res) => {
        wx.setStorageSync('USER_TOKEN', res.data.data.token);
        console.log(' USER_TOKEN Success! ');
      });
    }
  })
}
module.exports = {
  request: request,
}