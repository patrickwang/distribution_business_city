//支付功能
import api from '../utils/api'
//常量
let app = getApp();
let globalData = app.globalData;

function generateOrder(callback) {
  var that = this;
  //统一支付
  wx.showToast({
    title: '正在处理订单...',
    icon: 'loading',
    duration: 10000,
    mask: true
  })
  var orderNo = globalData.orderNo
  setTimeout(function () {
    api.request('/order/pay', 'POST', {
      'order_no': orderNo
    }, (res) => {
      console.log(res.data)
      var pay = res.data;
      //发起支付
      //支付
      payFn(pay,callback);
    });
  }, 2000)
}
/* 支付   */
function payFn(param, callback) {
  wx.requestPayment({
    timeStamp: param.timestamp,
    nonceStr: param.nonceStr,
    package: param.package,
    signType: param.signType,
    paySign: param.paySign,
    success: function (res) {
      wx.hideToast();
      wx.showModal({
        title: '提示',
        content: '支付成功',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            callback();
          }
        }
      })
    },
    fail: function (res) {
      // fail
      wx.hideToast();
      wx.showModal({
        title: '提示',
        content: '支付失败',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            wx.navigateBack();
          }
        }
      })
      return
    },
    complete: function () {
    }
  })
}
module.exports = {
  generateOrder: generateOrder
}